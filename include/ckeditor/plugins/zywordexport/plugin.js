﻿CKEDITOR.plugins.add('zywordexport',
{
	init: function(editor)
	{
		editor.addCommand('zywordexport',
		{
			exec: function(editor)
			{				
				zyOffice.getInstance().api.exportWord();
			}
		});
		editor.ui.addButton('zywordexport',
		{
			label: '导出Word文档（docx格式）',
			command: 'zywordexport',
			icon: this.path + 'images/exword.png'
		});
	}
});
