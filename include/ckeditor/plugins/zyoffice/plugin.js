﻿CKEDITOR.plugins.add('zyoffice',
{
	init: function(editor)
	{
		editor.addCommand('zyoffice',
		{
			exec: function(editor)
			{				
				zyOffice.getInstance().api.openDoc();
			}
		});
		editor.ui.addButton('zyoffice',
		{
			label: '导入Word文档（docx格式）',
			command: 'zyoffice',
			icon: this.path + 'images/w.png'
		});
	}
});
