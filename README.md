- zyOffice官网：http://www.ncmem.com/webapp/zyoffice/index.aspx
- 功能演示：https://www.ixigua.com/7347613964232032820?is_new_connect=0&is_new_user=0

#### 介绍
泽优Office文档转换服务（zyOffice）是由荆门泽优软件有限公司开发的一个支持多平台(Windows,macOS,Linux)的简化Word内容发布的组件。适用于政府门户，集约化平台，CMS，OA，博客，文档管理系统，微信公众号，微博，自媒体，传媒，在线教育等领域。主要帮助用户解决Word文档一键导入的问题，能够支持从ie6到chrome的全部浏览器和常用操作系统（Windows,MacOS,Linux）及信创和国产化环境（龙芯，中标麒麟，银河麒麟，统信UOS）。能够大幅度提高企业信息发布效率，帮助企业实现全媒体平台信息一体化战略。

#### 安装教程(服务端)

- Windows: http://www.ncmem.com/doc/view.aspx?id=82170058de824b5c86e2e666e5be319c

#### 成功案例

- 北京银联信科技股份有限公司
- 优慕课在线教育科技（北京）有限责任公司
- 西安工业大学
- 西安恒谦教育科技股份有限公司
- 西安德雅通科技有限公司
- 国家气象中心
- 国开泛在（北京）教育科技有限公司
- 北京大唐融合通信技术有限公司
- 北京思路创新科技有限公司
- 北京兴油工程项目管理有限公司
- 北京海泰方圆科技股份有限公司
